# IP Address Lookup

## Overview
Data can go through several “hops,” so you may need to trace an IP address to find out where your data is going. When you have the specific IP addresses where your information is sent, you can find out that location through an online database.

Tracing an IP address not only tells you where your data is going but also the location of another internet user and if data is going somewhere it shouldn’t. It can even help you problem-solve a poor connection to a host.
![Screenshot](public/screenshot.jpg)

## Project Demo
http://ip-address-lookup.vercel.app

## Project setup
```
pnpm install
```

### Compiles and hot-reloads for development
```
pnpm run serve
```

### Compiles and minifies for production
```
pnpm run build
```

### Lints and fixes files
```
pnpm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## Sponsors
[![JetBrains Logo](https://masihtak.com/portfolio/assets/img/sponsors/jetbrains.svg)](https://www.jetbrains.com/?from=https://gitlab.com/MasihTak/ip-address-lookup)
[![BitNinja Logo](https://masihtak.com/portfolio/assets/img/sponsors/bitninja_black.svg)](https://bitninja.io)
